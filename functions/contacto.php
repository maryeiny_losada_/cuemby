<?php

/**
 * Script encargado de recibir los datos
 * del formulario de contacto y enviar vial email
 * a maryeiny.losada@gmail.com
 */

session_start();

//Campos de nombre recibido por POST
if(empty($_POST['nombres']) == FALSE){
    $_SESSION['nombres'] = $_POST['nombres'];
}

//Campos de email recibido por POST
if(empty($_POST['email']) == FALSE){
    $_SESSION['email'] = $_POST['email'];
}

//Campos de asunto recibido por POST
if(empty($_POST['asunto']) == FALSE){
    $_SESSION['asunto'] = $_POST['asunto'];
}

//Campos de mensaje recibido por POST
if(empty($_POST['mensaje']) == FALSE){
    $_SESSION['mensaje'] = $_POST['mensaje'];
}

//Validación de campos no nulos
if(empty($_POST['nombres']) == FALSE && empty($_POST['email']) == FALSE && empty($_POST['asunto']) == FALSE && empty($_POST['mensaje']) == FALSE){
    $contenido = '';
    $nombres = $_POST['nombres'];
    $email = $_POST['email'];
    $asunto = $_POST['asunto'];
    $mensaje = $_POST['mensaje'];

    $para      = 'maryeiny.losada@gmail.com';
    $titulo    = 'Contacto - PerFit';
    $contenido .= 'Nombre de contacto: '. $nombres.' </br>';
    $contenido .= 'Correo electrónico: '. $email.' </br>';
    $contenido .= 'Mensaje: '. $mensaje.' </br>';

    //Envia el correo
    mail($para, $titulo, $contenido);

    unset($_SESSION['nombres']);
    unset($_SESSION['email']);
    unset($_SESSION['asunto']);
    unset($_SESSION['mensaje']);

    //Envia mensaje de exito
    $_SESSION['success'] = 'Se ha enviado su mensaje correctamente, en las próximas 48 horas nos estaremos contactando con usted.';
    header("Location: ../contacto.php");
}else{
    //Envia mensaje de error
    $_SESSION['error'] = 'Complete los campos solicitados.';
    header("Location: ../contacto.php");
}
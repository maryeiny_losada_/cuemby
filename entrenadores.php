<!DOCTYPE html>
<html lang="es">
<head>
    <?php
    include 'includes/header.php';
    ?>
</head>
<body>
<header class="header">
    <div class="width-100">
        <div class="menu-ppal container">
            <?php
            include 'includes/nav-inicial.php';
            ?>
        </div>

        <div class="body-content">
            <h1 class="text-center">

            </h1>
        </div>
    </div>
</header>

<div class="container">
    <h1 class="text-center margin-top-30 color-titulo">Entrenadores especializados</h1>

    <p class="text-center">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </p>

    <table class="table margin-top-50 margin-bottom-50">
        <thead>
        <tr>
            <th class="text-center" width="16%">Nombres</th>
            <th class="text-center" width="16%">Teléfono</th>
            <th class="text-center" width="16%">Cargo</th>
            <th class="text-center" width="16%">Especialidad</th>
        </tr>
        </thead>

        <tbody class="body-entrenadores">

        </tbody>
    </table>
</div>

<footer class="footer">
    <?php
    include 'includes/footer.php';
    ?>
</footer>

<?php
include 'includes/script.php';
?>
</body>
</html>
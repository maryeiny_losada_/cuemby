<!DOCTYPE html>
<html lang="es">
<head>
    <?php
        include 'includes/header.php';
    ?>
</head>
<body>
    <header class="header">
        <div class="width-100">
            <div class="menu-ppal container">
                <?php
                include 'includes/nav-inicial.php';
                ?>
            </div>

            <div class="body-content">
                <h1 class="text-center">

                </h1>
            </div>
        </div>
    </header>

    <?php
    include 'includes/slide.php';
    ?>


    <div class="content-pagina container">
        <h1 class="text-center margin-top-30 color-titulo">PerFit </h1>

        <p class="text-center">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </p>

        <h3 class="color-titulo margin-top-30">Horarios </h3>

        <table class="table margin-top-50 margin-bottom-50">
            <thead>
                <tr>
                    <th class="text-center" width="16%">Lunes</th>
                    <th class="text-center" width="16%">Martes</th>
                    <th class="text-center" width="16%">Miércoles</th>
                    <th class="text-center" width="16%">Jueves</th>
                    <th class="text-center" width="16%">Viernes</th>
                    <th class="text-center" width="16%">Sábado</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>5:00 - 7:00 Aeróbicos </td>
                    <td>5:00 - 7:00 Aeróbicos </td>
                    <td>5:00 - 7:00 Aeróbicos </td>
                    <td>5:00 - 7:00 Aeróbicos </td>
                    <td>5:00 - 7:00 Aeróbicos </td>
                    <td>5:00 - 7:00 Aeróbicos </td>
                </tr>
                <tr>
                    <td>7:00 - 9:00 Core </td>
                    <td>7:00 - 9:00 Core </td>
                    <td>7:00 - 9:00 Core </td>
                    <td>7:00 - 9:00 Core </td>
                    <td>7:00 - 9:00 Core </td>
                    <td>7:00 - 9:00 Core </td>
                </tr>
                <tr>
                    <td>9:00 - 11:00 Abdomen </td>
                    <td>9:00 - 11:00 Abdomen </td>
                    <td>9:00 - 11:00 Abdomen </td>
                    <td>9:00 - 11:00 Abdomen </td>
                    <td>9:00 - 11:00 Abdomen </td>
                    <td>9:00 - 11:00 Abdomen </td>
                </tr>
                <tr>
                    <td>11:00 - 13:00 Crosstech </td>
                    <td>11:00 - 13:00 Crosstech </td>
                    <td>11:00 - 13:00 Crosstech </td>
                    <td>11:00 - 13:00 Crosstech </td>
                    <td>11:00 - 13:00 Crosstech </td>
                    <td>11:00 - 13:00 Crosstech </td>
                </tr>
                <tr>
                    <td>20:00 - 22:00 Glúteos </td>
                    <td>20:00 - 22:00 Glúteos </td>
                    <td>20:00 - 22:00 Glúteos </td>
                    <td>20:00 - 22:00 Glúteos </td>
                    <td>20:00 - 22:00 Glúteos </td>
                    <td>20:00 - 22:00 Glúteos </td>
                </tr>
                <tr>
                    <td>14:00 - 16:00 Zumba </td>
                    <td>14:00 - 16:00 Zumba </td>
                    <td>14:00 - 16:00 Zumba </td>
                    <td>14:00 - 16:00 Zumba </td>
                    <td>14:00 - 16:00 Zumba </td>
                    <td>14:00 - 16:00 Zumba </td>
                </tr>
                <tr>
                    <td>16:00 - 18:00 Pierna </td>
                    <td>16:00 - 18:00 Pierna </td>
                    <td>16:00 - 18:00 Pierna </td>
                    <td>16:00 - 18:00 Pierna </td>
                    <td>16:00 - 18:00 Pierna </td>
                    <td>16:00 - 18:00 Pierna </td>
                </tr>
                <tr>
                    <td>18:00 - 20:00 Rumba </td>
                    <td>18:00 - 20:00 Rumba </td>
                    <td>18:00 - 20:00 Rumba </td>
                    <td>18:00 - 20:00 Rumba </td>
                    <td>18:00 - 20:00 Rumba </td>
                    <td>18:00 - 20:00 Rumba </td>
                </tr>
                <tr>
                    <td>15:00 - 14:00 Tren superior  </td>
                    <td>15:00 - 14:00 Tren superior  </td>
                    <td>15:00 - 14:00 Tren superior  </td>
                    <td>15:00 - 14:00 Tren superior  </td>
                    <td>15:00 - 14:00 Tren superior  </td>
                    <td>15:00 - 14:00 Tren superior  </td>
                </tr>
            </tbody>
        </table>
    </div>

    <footer class="footer">
    <?php
    include 'includes/footer-contact.php';
    ?>

    <?php
        include 'includes/footer.php';
    ?>
    </footer>

    <?php
    include 'includes/script.php';
    ?>
</body>
</html>
// Creamos la variable 'app' y le damos el nombre del modulo
var app = angular.module('listarUsuarios', []);

// Creamos el controllador 'postresController'
app.controller('usuariosController', function ($scope, $http) {
    // Mediante el servicio $http hacemos una petición 'get' a un archivo PHP que nos
    $http.get("assets/archivos/usuarios.json")
        .then(function (response) {
            $scope.usuarios = response.data.usuariosLista;
        });
});
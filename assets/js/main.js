$(document).ready(function(){
    $.ajax({
        url: "assets/archivos/entrenadores.json",
    }).done(function(respuesta){
        var html = '<tr>' +
            '<td colspan="4" class="text-center">No existen Entrenadores regitrados.</td>'+
            '</tr>';
        if(typeof respuesta != "undefined"){
            html = '';
            $.each( respuesta.entrenadores, function( key, value ) {
                html += '<tr>';
                html += '<td>'+value.nombre+'</td>';
                html += '<td>'+value.telefono+'</td>';
                html += '<td>'+value.cargo+'</td>';
                html += '<td>'+value.especialidad+'</td>';
                html += '</tr>';
            });
        }
        $('.body-entrenadores').html(html);
    });

    $.ajax({
        url: "assets/archivos/menu-ppal.json",
    }).done(function(menu){
        var content = '';
        var selected = '';
        var pagina = self.location.href;

        if(typeof menu != "undefined"){
            $.each( menu.item, function( key, value ) {
                if(pagina.indexOf(value.enlace) == -1){
                    selected = '';
                }else{
                    selected = 'nav-select';
                }
                content += '<li class="'+selected+'">';
                content += '<a title="'+value.nombre+'" href="'+value.enlace+'">'+value.nombre+'</a>';
                content += '</li>';
            });
        }
        $('.menu-content').append(content);
    });
})

function menuResponsive() {
    var nav = $('#nav-menu');
    if (nav.hasClass("topnav") == true) {
        if (nav.hasClass("responsive") == false) {
            nav.addClass('responsive');
            $('.opacidad').show();
            $('.icon-menu-text').addClass('aling-right');
            $(".aling-right").animate({
                width: "100%"
            }, 1000);
            $('.icon-menu-text').html('X');
        } else {
            $(".aling-right").animate({
                width: "10%"
            }, 1000, function(){
                nav.removeClass( "responsive");
                $('.icon-menu-text').html('&#9776;');
                $('.opacidad').hide();
            });
        }
    } else {
        nav.addClass('topnav');
    }
}
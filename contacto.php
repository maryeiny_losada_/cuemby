<!DOCTYPE html>

<?php
session_start();
?>

<html lang="es">
<head>
    <?php
    include 'includes/header.php';
    ?>
</head>
<body>
<header class="header">
    <div class="width-100">
        <div class="menu-ppal container">
            <?php
            include 'includes/nav-inicial.php';
            ?>
        </div>

        <div class="body-content">
            <h1 class="text-center">

            </h1>
        </div>
    </div>
</header>

<div class="container">
    <h1 class="text-center margin-top-30 color-titulo">Página de Contacto</h1>

    <p class="text-center">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
        magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
        pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
        laborum.
    </p>

    <div class="width-100 margin-bottom-100">
        <div class="width-50 left margin-right">
            <form id="contact" action="functions/contacto.php" method="post">
                <h4>Gracias por contactarnos</h4>

                <?php
                if (empty($_SESSION['error']) == FALSE): ?>
                    <p class="error"><?php echo $_SESSION['error']; ?></p>
                <?php endif; ?>

                <?php
                if (empty($_SESSION['success']) == FALSE): ?>
                    <p class="success"><?php echo $_SESSION['success']; ?></p>
                <?php endif; ?>

                <fieldset>
                    <input name="nombres" placeholder="Nombre completo" type="text" tabindex="1" required autofocus
                           value="<?php echo empty($_SESSION['nombres']) == FALSE ? $_SESSION['nombres'] : ''; ?>">
                </fieldset>
                <fieldset>
                    <input name="email" placeholder="Correo electrónico" type="email" tabindex="2" required
                           value="<?php echo empty($_SESSION['email']) == FALSE ? $_SESSION['email'] : ''; ?>">
                </fieldset>
                <fieldset>
                    <input name="asunto" placeholder="Asunto" type="text" tabindex="2"
                           value="<?php echo empty($_SESSION['asunto']) == FALSE ? $_SESSION['asunto'] : ''; ?>">
                </fieldset>
                <fieldset>
                    <textarea name="mensaje" placeholder="Mensaje" tabindex="5"
                              required><?php echo empty($_SESSION['mensaje']) == FALSE ? $_SESSION['mensaje'] : ''; ?></textarea>
                </fieldset>
                <fieldset class="margin-top-15">
                    <button name="submit" class="submit" type="submit">Enviar</button>
                </fieldset>
            </form>
            <div class="clear-both"></div>
        </div>

        <div class="width-50 right margin-left">
            <iframe class="iframe-contact"
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.305955770134!2d-75.59415368568659!3d6.2233285954950235!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e4429c1538306ed%3A0x1ccb5282fee31e56!2sCra.+70+%2319-65%2C+Medell%C3%ADn%2C+Antioquia!5e0!3m2!1ses!2sco!4v1510519835673"
                    height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
            </iframe>
        </div>

        <div class="clear-both"></div>
    </div>
</div>

<?php
session_destroy();
?>

<footer class="footer">
    <?php
    include 'includes/footer.php';
    ?>
</footer>

<?php
include 'includes/script.php';
?>
</body>
</html>
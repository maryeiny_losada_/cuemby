<nav class="display-flex">
    <div class="left margin-right">
        <img class="logo" src="assets/img/logo.png"/>
    </div>
    <div id="nav-menu" class="menu-nav right margin-left topnav">
        <ul class="nav menu-content">
            <li class="icon-menu">
                <a href="javascript:void(0);" class="icon-menu-text" onclick="menuResponsive()">&#9776;</a>
            </li>
        </ul>
    </div>
</nav>
<div class="opacidad"></div>
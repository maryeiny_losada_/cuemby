<div class="social width-100 clear-both">
    <div class="width-50 color-white left">
        <h3 class="color-titulo titulo-footer">
            Siguenos en
        </h3>

        <div>
            <a class="color-titulo margin-right-20" href="https://www.facebook.com/" target="_blank">
                <img class="ico-social" src="assets/img/iconos/facebook.png" /> &nbsp;Perfit
            </a>
            <a class="color-titulo margin-right-20" href="https://plus.google.com/?hl=es" target="_blank">
                <img class="ico-social" src="assets/img/iconos/googleplus.png" /> &nbsp;Perfit
            </a>
            <a class="color-titulo margin-right-20" href="https://twitter.com/" target="_blank">
                <img class="ico-social" src="assets/img/iconos/twitter.png" /> @perfit
            </a>
        </div>
    </div>
    <div class="width-50 color-white right footer-border-left">
        <h3 class="color-titulo">
            Contactenos
        </h3>

        <div class="color-titulo">
            <a class="color-titulo margin-right-20" href="mailto:contacto@perfit.com" target="_blank">
                <img src="assets/img/iconos/mail.png" /> contacto@perfit.com
            </a>
            <a class="color-titulo margin-right-20 margin-top-10" href="tel:4011121" target="_blank">
                <img src="assets/img/iconos/phone.png" /> (1) 401 11 21
            </a>
            </br>
            <img class=" margin-top-10" src="assets/img/iconos/direction.png" /> Carrera 70 # 19 - 65 - Medellín Antioquia, Colombia
        </div>
    </div>
    <div class="clear-both"></div>
</div>